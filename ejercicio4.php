<?php
    function reemplazar($file, $newFile, $searchText, $replaceText) {
        $text = file_get_contents($file);
        $newText = str_ireplace($searchText, $replaceText, $text);
        // Si la carpeta no existe, la crea
        if (is_dir("./assets") != true) {
            mkdir("./assets");
        }
        file_put_contents("./assets/$newFile", $newText);
    }
    reemplazar("quijote.txt", "quijote-modificado.txt", "Sancho", "Morty");
?>